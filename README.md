/ 20200226053622
// http://localhost:8080/users?format=json

{
  "msg": "success",
  "code": "0000",
  "data": [
    {
      "id": 1,
      "name": "Dara_01",
      "male": "Male",
      "email": "dara_01@gmail.com",
      "address": "PP1",
      "links": [
        {
          "rel": "self",
          "href": "http://localhost:8080/users/1"
        }
      ]
    },
    {
      "id": 2,
      "name": "Dara_02",
      "male": "Male",
      "email": "dara_02@gmail.com",
      "address": "PP2",
      "links": [
        {
          "rel": "self",
          "href": "http://localhost:8080/users/2"
        }
      ]
    },
    {
      "id": 3,
      "name": "Dara_03",
      "male": "Male",
      "email": "dara_03@gmail.com",
      "address": "PP3",
      "links": [
        {
          "rel": "self",
          "href": "http://localhost:8080/users/3"
        }
      ]
    },
    {
      "id": 4,
      "name": "Dara_04",
      "male": "Male",
      "email": "dara_04@gmail.com",
      "address": "PP4",
      "links": [
        {
          "rel": "self",
          "href": "http://localhost:8080/users/4"
        }
      ]
    },
    {
      "id": 5,
      "name": "Dara_05",
      "male": "Male",
      "email": "dara_05@gmail.com",
      "address": "PP5",
      "links": [
        {
          "rel": "self",
          "href": "http://localhost:8080/users/5"
        }
      ]
    }
  ]
}