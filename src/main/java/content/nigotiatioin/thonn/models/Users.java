package content.nigotiatioin.thonn.models;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.ControllerLinkBuilder;

public class Users extends RepresentationModel<Users> {

    private Integer id;
    private String name;
    private String male;
    private String email;
    private String address;

    public Users() {
    }

    public Users(Integer id, String name, String male, String email, String address) {
        this.id = id;
        this.name = name;
        this.male = male;
        this.email = email;
        this.address = address;
        this.add(new Link("http://localhost:8080/users/"+this.getId()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMale() {
        return male;
    }

    public void setMale(String male) {
        this.male = male;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
